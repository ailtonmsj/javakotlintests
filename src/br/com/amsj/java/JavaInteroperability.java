package br.com.amsj.java;

import br.com.amsj.kotlin.exploring.Customer;
import br.com.amsj.kotlin.exploring.CustomerDatabase;

import java.util.List;

public class JavaInteroperability {

    public static void main(String... args){

        Customer phil = new Customer(0, "Phil", "Collins");
        System.out.println(phil);

//        Customer test1 = new Customer (null, null); // Doesnt compile because Long in kotlin is long in Java
//        Customer test2 = new Customer(0, null, null); // Compile but throws a RuntimeException because is Illegal pass null on that type of variable
        Customer test3 = new Customer(1, "Grinch", null); // The last argument accepts null because the value declaring on the constructor is String?
        System.out.println(test3);


        CustomerDatabase customerDatabase = new CustomerDatabase();
        List<Customer> customers = customerDatabase.getCustomers();
        System.out.println(customers);

        try {
//            customers.add(new Customer(4, "Bruce", "Banner")); // Is a immutable list, so is not possible change the value of the list
            customerDatabase.addCustomer(new Customer(4, "Bruce", "Banner")); // Is a immutable list, so is not possible change the value of the list
        }catch (IllegalAccessException e){
            System.out.println("Exception caught");
        }

        CustomerDatabase.Companion.helloStaticCompanion();
        CustomerDatabase.helloStaticJvm();


    }

}
