package br.com.amsj.kotlin.exploring

import java.io.FileInputStream

@Throws(InterruptedException::class)
fun divide(a: Int, b: Int): Double {
    Thread.sleep(500)
    return (a.toDouble() / b)
}

fun printFile() {
    val input = FileInputStream("file.txt")

    input.use {
        // do something, if a exception is throw the resource will be close

    }

}


fun main(args: Array<String>) {

    printFile()

    println(divide(0,0))
    println(divide(10,0))
    println(divide(10,1))

    val result = try {
        divide(50, 0)
    }
    catch (e: Exception) {
        println(e)
        0
    }

    println(result)
}