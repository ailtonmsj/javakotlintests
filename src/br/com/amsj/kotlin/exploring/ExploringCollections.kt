package br.com.amsj.kotlin.exploring

fun main(args: Array<String>) {

//    val colors = listOf("RED", "BLUE", "GREEN")

    val colors2 = ArrayList<String>()
    colors2.add("PURPLE")
    colors2.add("YELLOW")
    colors2.add("BLACK")
//
//    println(colors::class.qualifiedName)
//    println(colors2::class.qualifiedName)
//
//    println(colors)
//    println(colors2)
//
//    colors2.removeAt(0)
//    println(colors2)

//    val days = mutableListOf("Monday", "Tuesday", "Wednesday")
//    days.add("Thurday")
//    println(days)
//
//    val months = mutableListOf<Int>()
//    months.add(0)
//    months.add(1)
//    println(months)

//    val monthSet = mutableSetOf("January", "February", "March")
//    monthSet.add("February")
//    println(monthSet)

    val monthMap = mutableMapOf("R" to "red", "B" to "blue", "G" to "green")
//    println(monthMap)

    val intArray = intArrayOf(1,2,3,4,5)
    intArray.set(0, 6)

    intArray.forEach { println(it) }


}