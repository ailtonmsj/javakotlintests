package br.com.amsj.kotlin.exploring

import br.com.amsj.kotlin.exercise1.KotlinPerson

fun main(args: Array<String>) {

    val people = HashMap<Int, KotlinPerson>()
    people.put(1, KotlinPerson(1, "Mrs", "Jane", "Joplin", null))
    people.put(2, KotlinPerson(2, "Mr", "Michael", "Jordan", null))
    people.put(3, KotlinPerson(3, "Mrs", "Tarja", "Turunem", null))

    (0 until 3).forEach { println(people.get(it)) }

//    for((key, value) in people)
//        println("chave $key, valor $value")

//    (0..3).forEach { println(it) }
//    (3 downTo 0).forEach{ println(it) }
//    (0 until 3).forEach{ println(it) }
//    (0..9 step 2).forEach{ println(it)}
//    ('A'..'F').forEach { println(it) }
}