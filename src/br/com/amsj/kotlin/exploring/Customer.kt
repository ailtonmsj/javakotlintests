package br.com.amsj.kotlin.exploring

data class Customer(val id: Long, val name: String, val surname: String?){

    override fun toString(): String {
        return "$id - $name"
    }
}

class CustomerDatabase(){

    val customers = listOf(Customer(1, "Godofredo", "Silva"),
            Customer(2, "Alfred", "Burton]"),
            Customer(3, "Selina", "kyle"))

    @Throws(IllegalAccessException::class)
    fun addCustomer(c: Customer){
        throw IllegalAccessException("It's not possible add a Customer!");
    }

    companion object {
        fun helloStaticCompanion(){
            println("static companion")
        }

        @JvmStatic
        fun helloStaticJvm(){
           println("static jvm")
        }

    }

}



