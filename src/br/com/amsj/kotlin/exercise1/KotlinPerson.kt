package br.com.amsj.kotlin.exercise1

import java.util.*

data class KotlinPerson(val id: Long, val title: String, val firstName: String, val surname: String, val dateOfBirth: Calendar?){

    override fun toString() =
            "$title $firstName $surname"

    val age: Int?
    get() = getAge(dateOfBirth)

    val safeAge: Int
        get() = age ?: -1

    fun getColorUpperCaseColor() : String {
        return favoriteColor?.toUpperCase() ?: ""
    }

    var favoriteColor: String? = null

    fun getLastLetter(a: String) = a.takeLast(1)

    fun getLastLatterFavoriteColor() : String {
        return favoriteColor?.let {
            getLastLetter(it)
        } ?: ""
    }

    fun getColorType() : String {
        val color = getColorUpperCaseColor()
//        return if(color == "")
//            "empty"
//        else if(color == "RED" || color == "BLUE" || color == "GREEN")
//            "rgb"
//        else
//            "other"

        return when(color){
            "" -> "empty"
            "BLUE", "GREEN", "RED" -> "rgb"
            else -> "other"
        }

    }



    override fun equals(other: Any?): Boolean {
        if(this == other) return true
        if(other != null && !(other is KotlinPerson)) false

        val that = other as KotlinPerson
        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.title, that.title) &&
                Objects.equals(this.firstName, that.firstName) &&
                Objects.equals(this.surname, that.surname)
    }

    override fun hashCode() = Objects.hash(id, title, firstName, surname)

    companion object {
        fun getAge(dateOfBirth: Calendar?): Int?{
            if(dateOfBirth == null) return -1

            val today = GregorianCalendar()
            val years = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR)
            return if(dateOfBirth.get(Calendar.DAY_OF_YEAR) >= today.get(Calendar.DAY_OF_YEAR)) years -1 else years
        }
    }
}


fun main(args: Array<String>) {

    val person3 = KotlinPerson(3, "Mr", "Poguer", "Master", null)
    person3.favoriteColor = String(StringBuilder("red"))

    println("Equality color ${person3.getColorType()}")

}