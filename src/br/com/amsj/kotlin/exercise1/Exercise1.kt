package br.com.amsj.kotlin.exercise1

import java.util.*
import javax.lang.model.type.NullType

fun main(args: Array<String>) {

    val john = KotlinPerson(1L, "Mr", "John", "Blue", GregorianCalendar(1977,9,3))
    val jane = KotlinPerson(2L, "Mrs", "Jane", "Green", null)

    println("Kotlin")
    println("$john 's age is ${john.age}")
    println("$jane 's age is ${jane.age}")
    println("The age of someone born on 3rd May 1988 is ${KotlinPerson.getAge(GregorianCalendar(1988,5,3))}")
    val olderPerson = if(john.safeAge > jane.safeAge) john else jane
    println("The older person is $olderPerson")
}