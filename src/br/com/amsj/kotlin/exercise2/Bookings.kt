package br.com.amsj.kotlin.exercise2

interface BookingManager {

    val version: String

    fun reserveSeat(seat: Seat, customerId: Long): Boolean

    fun isSeatFree(seat: Seat): Boolean

    fun systemStatus() = "All functions are functional"
}

open class BasicBookingManager(authorizationKey: String) : BookingManager {

    override val version = "1.0"

    override fun isSeatFree(seat: Seat) = true

    override fun reserveSeat(seat: Seat, customerId: Long) = false
}

class AdvancedBookingManager(key: String) : BasicBookingManager(key), java.io.Closeable{

    init {
        if(!key.equals("123456")) throw UnathorizedUserException()
    }

    override val version = "1.2"

    fun howManyBookings() = 10

    override  fun close(){
        println("close")
    }
}

fun String.firstUppercase(): String {
    println("custom function on String")
    return this[0].toUpperCase() + this.substring(1)
}

class UnathorizedUserException() : Throwable()

fun main(args: Array<String>) {

    println(AdvancedBookingManager("123456").howManyBookings())

    val message = "welcome to the system";
    println(message.firstUppercase())
}

