package br.com.amsj.kotlin.exercise2

import java.math.BigDecimal

data class Seat(val row: Int, val num: Int, val price: BigDecimal, val description: String) {
	//YOU MAY NOT EDIT THIS CLASS!
    override fun toString(): String = "Seat $row-$num $$price ($description)"
}

class Theater() {

    val rowQtde = 15
    val seatPerRow = 36

    val SEAT_NAME_BACK_ROW = "Back row"
    val SEAT_NAME_CHEAPER_SEAT = "Cheaper seat"
    val SEAT_NAME_RESTRICTED_VIEW = "Restricted View"
    val SEAT_NAME_BEST_VIEW = "Best view"
    val SEAT_NAME_STANDART = "Standard seat"

    val SEAT_PRICE_BACK_ROW = BigDecimal(14.50)
    val SEAT_PRICE_CHEAPER_SEAT = BigDecimal(14.50)
    val SEAT_PRICE_RESTRICTED_VIEW = BigDecimal(16.50)
    val SEAT_PRICE_BEST_VIEW = BigDecimal(21.00)
    val SEAT_PRICE_STANDART = BigDecimal(18.00)

   // SEAT PRICES:
   // Seats in rows 14 and 15 cost 14.50
   // Seats in rows 1 to 13 and numbered 1 to 3 or 34 to 36 cost 16.50
   // All other seats in row 1 cost 21.00
   // All other seats cost 18.00
   
   // SEAT DESCRIPTIONS:
   // Seats in row 15: "Back row"
   // Seats in row 14: "Cheaper seat"
   // All other rows, seats 1 to 3 and 34 to 36: "Restricted View"
   // All other seats in rows 1 and 2 "Best view"
   // All other seats: "Standard seat"

    fun loadList(): List<Seat>{

        val seatsLoad = ArrayList<Seat>()

        for (row in 1..rowQtde){
            (1..seatPerRow).forEach{
                val seatNumber = it
                val seatDescription = getSeatDescription(row, seatNumber)
                val seatPrice = getSeatPrice(row, seatNumber)
                seatsLoad.add(Seat(row, seatNumber, seatPrice, seatDescription))
            }
        }
        return seatsLoad
    }

    fun getSeatPrice(row: Int, seatNumber: Int): BigDecimal {

        return if(row == 15)
            SEAT_PRICE_BACK_ROW
        else if(row == 14)
            SEAT_PRICE_CHEAPER_SEAT
        else if(seatNumber <= 3 || seatNumber >= 34)
            SEAT_PRICE_RESTRICTED_VIEW
        else if(row == 1 || row == 2)
            SEAT_PRICE_BEST_VIEW
        else
            SEAT_PRICE_STANDART
    }

    fun getSeatDescription(row: Int, seatNumber: Int): String{

        return when {
            row == 15 -> SEAT_NAME_BACK_ROW
            row == 14 -> SEAT_NAME_CHEAPER_SEAT
            seatNumber <= 3 || seatNumber >= 34 -> SEAT_NAME_RESTRICTED_VIEW
            row == 1 || row == 2 -> SEAT_NAME_BEST_VIEW
            else -> SEAT_NAME_STANDART
        }
    }
	var seats = loadList()
}

fun main(args: Array<String>) {
    val cheapSeats = Theater().seats.filter {it.price == BigDecimal(14.50)}
    for (seat in cheapSeats) println (seat)
}